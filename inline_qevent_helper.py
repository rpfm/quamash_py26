from PyQt4.QtCore import QEventLoop
import trollius as asyncio
import functools

def inline_async_done( qloop, task):
        
    assert type(qloop) == QEventLoop
    qloop.quit()

def inline_async( coro, *args):
    qloop = QEventLoop()
    task = asyncio.ensure_future(coro(*args))
    task.add_done_callback(functools.partial(inline_async_done, qloop))
    qloop.exec_()
    return task.result()
